function productGallery(elem) {
  const gallery = document.querySelector(elem);
  const main = gallery.querySelector('[data-gallery-main]');

  function isActive(elem) {
    if ( elem.classList.contains('active') ) {
      return true;
    }

    return false;
  }

  function init(event) {
    event.preventDefault();
    let target = event.target.closest('[data-gallery-thumb]');
    if (!target || isActive(target)) return;

    main.src = target.href;
    main.alt = target.title;
    gallery.querySelectorAll('[data-gallery-thumb]').forEach((item) => {
      item.classList.remove('active');
    })
    target.classList.add('active');
  }

  gallery.addEventListener('click', init);
}

