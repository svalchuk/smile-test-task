require('dotenv').config();
const { src, dest, parallel, series, watch } = require('gulp');
const autoprefixer    = require('autoprefixer');
const bs              = require('browser-sync').create();
const cleanCSS        = require('gulp-clean-css');
const concat          = require('gulp-concat');
const nunjucksRender  = require('gulp-nunjucks-render');
const postcss         = require('gulp-postcss');
const rename          = require('gulp-rename');
const scss            = require('gulp-sass');
const sourcemaps      = require('gulp-sourcemaps');
const terser          = require('gulp-terser');

const paths = {
  styles: {
    src: './src/scss/**/*.scss',
    main: './src/scss/main.scss',
    dest: './public/assets/css/'
  },
  js: {
    src: './src/js/**/*.js',
    dest: './public/assets/js/'
  },
  templates: {
    path: './src/',
    dest: './public/'
  }
};

const nunjucksData = {
  BASE_URL: process.env.LOCAL_URL
};

const nunjucksBuildData = {
  BASE_URL: process.env.PAGES_URL
};

function styles() {
  return src(paths.styles.main)
    .pipe(sourcemaps.init())
    .pipe(scss().on('error', scss.logError))
    .pipe(postcss([autoprefixer()]))
    .pipe(cleanCSS())
    .pipe(rename({
      basename: 'main',
      suffix: '.min'
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(dest(paths.styles.dest))
    .pipe(bs.stream());
};

function templates () {
  return src(`${paths.templates.path}[^_]*.html`)
    .pipe(nunjucksRender({
      data: nunjucksData,
      path: [paths.templates.path]
    }))
    .pipe(dest(paths.templates.dest))
    .pipe(bs.stream());
};

function build_templates () {
  return src(`${paths.templates.path}[^_]*.html`)
    .pipe(nunjucksRender({
      data: nunjucksBuildData,
      path: [paths.templates.path]
    }))
    .pipe(dest(paths.templates.dest))
    .pipe(bs.stream());
};

function js() {
  return src(paths.js.src)
    .pipe(sourcemaps.init())
    .pipe(terser())
    .pipe(concat('main.min.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(dest(paths.js.dest))
    .pipe(bs.stream());
}

function serve() {
  bs.init({
    server: "./public",
    open: false
  });
  watch(paths.styles.src, styles);
  watch(paths.js.src, js);
  watch(`${paths.templates.path}*.html`, templates);
};

exports.styles = styles;
exports.templates = templates;
exports.build_templates = build_templates;
exports.serve = serve;
exports.default = series(parallel(styles, templates, js), serve);
exports.build = series(styles, build_templates, js);
